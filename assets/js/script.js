const openIcon = document.querySelector('.icon');
const linksWrapper = document.querySelector('.links');
const back =  document.querySelector('.back');
const closeIcon = document.querySelector('.close-btn');



openIcon.addEventListener('click', () => {
	linksWrapper.classList.add('open');
});


closeIcon.addEventListener('click', () => {
	linksWrapper.classList.remove('open');
});

back.addEventListener('click', () => {
	linksWrapper.classList.remove('open');
});